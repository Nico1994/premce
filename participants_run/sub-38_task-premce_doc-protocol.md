# PrEMCE. Protocol.

## ID: __________________________________

Day 1 |--------------------- | Day 2 | --------------------- | Day 3 | --------------------- 
----- | ----- | ----- | ----- | ----- | -----
Date: |				| Date: | 				| Date: | 
Starting time: | 	| Starting time: | 		| Starting time: |
Ending time: |		| Ending time: |		| Ending time: |
VL name: |			| VL name: |			| VL name: |

## Before Session on Day 1
### Documents available
- [ ] Signed **Informed consent**.
- [ ] Participant's **experiment links**.
- [ ] Participant's **session protocol**.
- [ ] Started a meeting in jitsi.

## During Session on Day 1
- [ ] Ask about general well-being. Comments?:
- [ ] The participant is using a desktop computer or a laptop on a desk/table.
- [ ] The participant is sitting down on a chair.
- [ ] The participant is on a quiet room.
- [ ] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [ ] Yes
- [ ] No. Comments: The participant complained about...


### ses 01 - part 01
- [ ] Send **ses 01 - part 01** link.

Understood the task?
- [ ] Yes
- [ ] No. Comments:

## Before Session on Day 2
### Documents available
- [ ] Participant's **experiment links**.
- [ ] Participant's **session protocol**.
- [ ] Started a meeting in jitsi.

## During Session on Day 2
- [ ] Ask about general well-being. Comments?:
- [ ] The participant is using a desktop computer or a laptop on a desk/table.
- [ ] The participant is sitting down on a chair.
- [ ] The participant is on a quiet room.
- [ ] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [ ] Yes
- [ ] No. Comments:

### Phase 2
- [ ] Send **ses 02 - part 01** link.

Understood the task?
- [ ] Yes
- [ ] No. Comments:

### Phase 3
- [ ] Send **ses 02 - part 02** link.

Understood the task?
- [ ] Yes
- [ ] No. Comments:

