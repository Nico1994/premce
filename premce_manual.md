# PrEMCE. Manual.

## Study info
This paradigm aims at exploring the relationship between expectations and memory formation in conditions where the expectations are violated as a consequence of a change in the environment. 

The experiment is structured into three phases: 
1) Session 01 - Part 01. Learning of contingencies A in day 1 (after a brief practice task to familiarise with the stimuli). Session 01, part 01 
2) Session 02 - Part 01. Switching contingencies A with B, and againg with A and B in day 2, after a small warm-up task with few trials. Session 02, part 01. 
3) Session 02 - Part 02. Testing memory for items (i.e., recognition memory test for incidental items). Session 02, part 02

- The contingencies refers to the butterfly-flower associations, which occurs in every set (A-B). Contingency levels are kept 85%, 5%, 5%, 5%. 
- Memory tests include: object recognition memory and confidence judgement 

## Testing platform
Pavlovia.


## Setting up an appointment
Recruitment for the study works over Prolific. Please have a look at https://gitlab.com/Soph87/LISCOlab/-/wikis/LISCOLabManual#prolific-recruitment-workflow for more information on that.
Once the participant has signed up correctly in Doodle (three sessions on three consecutive days) and he/she has inserted his/her Prolific ID, confirm it with a quick message in prolific.
24 hours before the first session: send a message in Prolific as a reminder and also send the Jitsi-meeting-link.


## Day 1
### Before the session
Check the following before meeting the participant.
- You have the **informed consent link**. https://forms.gle/WhUS7v4p2nappGn76
- You have the needed participant's **experiment links**.
- You have located the participants's **session protocol** on gitlab.
- Conection is stable on jitsi.

### During the session
- Meet-up in jitsi ([see here how to](https://gitlab.com/Soph87/LISCOlab/-/wikis/LISCOLabManual#guide-for-online-testing)).
## During Jitsi meeting on day 1:
  - **Welcome participant**
  - **Thank you for your participation**
  - **How are you today? Do you feel ready to participate?**  
  
  - **Send informed consent link via jitsi chat**
  - **Wait for participant to fill it out**
  
  - **Make sure the setting is okay:** 
    - Are you sitting in a quiet room at a table? Are you using a Computer/ Laptop not a Tablet etc.?  
    - Make sure that you won't get disturbed during the session.  
  
  - **Tell them about the structure and procedure of the experiment:** 
    - The experiment consists of 3 parts
    - You will do the first part today and the 2nd and 3rd part tomorrow, approximately at the same hour. 
    - The session today will last about 15 minutes, the session tomorrow will last about 45 minutes. 
    - Each session will start with a meeting in Jitsi and it will end with a meeting in Jitsi. I will stay in the videocall and you can use the same link to come back once you have finished or if any problems occur.
    - It is **important** that you stay focussed during the task. Put your phone aside and rather go to the bathroom beforehand. The single sessions do not last very long and we have planned short breaks to use the bathroom if necessary. Please, try to only leave your computer during those breaks since stopping halfway through one block might ruin the entire session.
    
  - **Send session 01 - part 01 Link**
  - Ask if the task is downloading. If yes, proceed. If no, try again and/or contact Francesco.
  
  - **Provide instructions for session 01 - part 01:**  
 
    - You will learn about butterflies and flowers.
    - Every butterfly feeds preferably on one of four flowers. Your task will be to predict on which flower each butterfly will feed on. 
    - From the time when the flowers appear, you have up to 3 seconds to make your choice. If no choice is made, the trial will be considered as incorrect, and the study will continue. Therefore, please try to respond as quickly as possible.  
    - After choosing a flower, a square will tell you if your choice was correct (green square) or not (red square)
    - In addition, please remember to press the H button every time the image that is presented at the center of the screen is a butterfly. Please try to do this after selecting the flower. 
    - Before you start you will be provided again with the detailed instructions, and you will be asked to repeat these instructions back to me afterwards. 
    - Although at the beginning it might be difficult, you will learn over time. Please do not feel bad if you make some mistakes even by the end of the sessions.
    - You will start by practicing on few trials. After those, you will be invited to work on the actual task. 
    - The keys you are going to need are the arrow keys, in the top right corner of your keyboard.
    - You have to press them, using your right fingers.
    - Please, leave your fingers over the keys during the entire task; this will make your responses faster.
    - At the end of each block (practice, task) you will be also asked to either continue or do the previous block again. Please return to the Jitsi meeting room when this question appears. 
    - Once you have finished Phase 1 return to the Jitsi meeting room.   
  
  - **Do you have any further questions?** 
  
  
## After session 1 on day 1:
  - How was it for you? 
  - Any comments? (Note potential comments in protocol)
    

## Day 2 
### Before the session
Check the following before meeting the participant.
- Participant links.
- Protocol sheet on gitlab.
- Conection is stable on jitsi.

### During the session
- Meet-up in jitsi ([see here how to](https://gitlab.com/Soph87/LISCOlab/-/wikis/LISCOLabManual#guide-for-online-testing)).
- Ask about general well-being.

### Session 02 - part 01

## During Jitsi meeting on day 2: 
  - **Welcome back**
  - **How are you today? Do you feel ready to participate?**
  
  - **Make sure the setting is okay:** 
    - Are you sitting in a quiet room at a table? Do you use a Computer/ Laptop not a Tablet etc.?  
    - Make sure that you won't get disturbed during the session. 
    
  - **Tell them about the structure and procedure of todays' experiment:**
    - Today you are going to do two parts.
    - Each part will last about 20 Minutes.
    - After you have completed the first part, you should come back to the Jitsi meeting to get further instructions. 
    - It is **important** that you stay focused during the task. Put your phone aside and rather go to the bathroom beforehand. Since there are two parts today, you can go to the bathroom between the two parts when there is a short break.
    
  - **Send Session 02 - part 01 Link**
  - Ask if the task is downloading. If yes, proceed. If no, try again and/or contact Francesco.
  
  - **Provide instructions for Session 02 - part 01:**  
    - Short recap: Yesterday you have learned about butterflies and flowers.
    - Today you will do the same task as the one you have done yesterday. Therefore, you should try to predict on which flower each butterfly will go. 
    - From the time when the flowers appear, you have up to 3 seconds to make your choice. If no choice is made, the trial will be considered as incorrect, and the study will continue. Therefore, please try to respond as quickly as possible.  
    - After choosing a flower, a square will tell you if your choice was correct (green square) or not (red square)
    - In addition, please remember to press the H button every time the image that is presented at the center of the screen is a butterfly.  
    - Before you start you will be provided again with the detailed instructions, and you will be asked to repeat these instructions back to me afterwards.
    - The keys you are going to need are the arrow keys.
    - You have to press them, using your right fingers.
    - Please, leave your fingers over the keys during the entire task; this will make your responses faster.
    - Before you start you will be provided again with the detailed instructions. 
    - Once you have finished Phase 2 return to the Jitsi meeting room.  
  
  - **Do you have any further questions?** 
  
## Participant returns to Jitsi meeting for 3rd phase (session 02 - part 02) instructions (: 
  - How was it for you? 
  - Any comments? (Note potential comments in protocol)
  
  - **Send Phase session 02 - part 02 Link**
  - Ask if the task is downloading. If yes, proceed. If no, try again and/or contact Javier.
    
  - **Provide instructions for session 02 - part 02:**  
    - Now we would like to know which object you have memorized.
    - We will show you a few objects and you will need to tell us:
      - Have you seen that object before? Use the arrow keys and your right hand.
      - How sure are you? Use the number keys from 1 to 4 on the top part of your keyboard and your left hand.
      - In which place did you see the object? Use the number keys from 1 to 4 on the top part of your keyboard and your left hand.
      - In which corner of the screen did you see the object. Use the number keys from 1 to 4 on the top part of your keyboard and your left hand.

    - Once you have finished ession 02 - part 02, return to the Jitsi meeting room. 

  - **Provide them with Prolific's link**



